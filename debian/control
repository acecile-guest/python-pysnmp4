Source: python-pysnmp4
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Jan Lübbe <jluebbe@debian.org>,
	   Deepak Tripathi <apenguinlinux@gmail.com>,
           Vincent Bernat <bernat@debian.org>
Standards-Version: 4.1.2
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3-sphinx, python3-sphinx-rtd-theme,
               python3-all,
	       python3-setuptools,
               python3-pycryptodome,
               python3-twisted,
               python3-pysmi,
               python3-pyasn1 (>= 0.2.3)
Homepage: https://github.com/pysnmp/pysnmp
Vcs-Git: https://salsa.debian.org/python-team/packages/python-pysnmp4.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-pysnmp4
Testsuite: autopkgtest-pkg-python

Package: python3-pysnmp4
Architecture: all
Depends: ${python3:Depends},
	 ${misc:Depends}
Description: Python SNMP library for agents and managers (Python 3 module)
 This  is a  Python implementation  of SNMP  v.1/v.2c/v.3  engine. Its
 general  functionality  is   to  assemble/disassemble  SNMP  messages
 from/into given SNMP Object  IDs along with associated values. PySNMP
 also provides a few transport methods specific to TCP/IP networking.
 .
 PySNMP is written entirely in  Python and is self-sufficient in terms
 that it does not rely on any third party tool (it isn't a wrapper).
 .
 This package provides Python 3 module.

Package: python-pysnmp4-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Multi-Arch: foreign
Description: Python SNMP library for agents and managers (unstable branch)
 This  is a  Python implementation  of SNMP  v.1/v.2c/v.3  engine. Its
 general  functionality  is   to  assemble/disassemble  SNMP  messages
 from/into given SNMP Object  IDs along with associated values. PySNMP
 also provides a few transport methods specific to TCP/IP networking.
 .
 PySNMP is written entirely in  Python and is self-sufficient in terms
 that it does not rely on any third party tool (it isn't a wrapper).
 .
 This package contains the documentation for PySNMP.
